# Parser3 класс для работы с MS SQL Server через Freetds под Linux
---

Для работы необходимо установить [Freetds](https://www.freetds.org/). Неплохая инструкция для Debian [тут](https://sysadminmosaic.ru/freetds/freetds). Для CentOS меняются только названия пакетов: `yum install unixODBC freetds`. Делаем freetds&#46;sh исполняемым и указываем путь к его директории через опцию **shellScriptDir**, если он будет размещен в другом месте.

### Инициализация
```php
^use[/classes/freetds/freetds.p]

$freetds[^Freetds::create[
	$.host[xx.xx.xx.xx]
	$.port[1433]
	$.database[xxx]
	$.user[xxx]
	$.password[xxx]
	$.locale[ru_RU.UTF8] ^rem{ необязательно }
	$.shellScriptDir[/../shell-scripts] ^rem{ необязательно, по умолчанию "./" }
]]
```

### Методы

**table**

```php
$table[^freetds.table{
	SELECT id, name, email
	FROM users
	WHERE cityID = 13
}]
```

**hash**

```php
$hash[^freetds.hash{
	SELECT id, name, email
	FROM users
	WHERE cityID = 13
}]
```

**string**

```php
$string[^freetds.string{
	SELECT name
	FROM users
	WHERE email = 'email@site.org'
}]
```

**int**

```php
$int(^freetds.int{
	SELECT cityID
	FROM users
	WHERE id = 13
}]
```

**double**

```php
$double(^freetds.double{
	SELECT salary
	FROM users
	WHERE id = 13
}]
```
