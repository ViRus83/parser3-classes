#!/bin/sh

# $1 - host
# $2 - port
# $3 - db
# $4 - user
# $5 - password
# $6 - sql
# $7 - locale [optional]

if ! [ -z ${7+x} ]; then
	export LANG=$7;
fi;

tsql -H $1 -p $2 -U $4 -P $5 -oq -r '%row%' -t '%col%' <<EOS
USE $3
GO
$6;
GO
exit
EOS
