# Parser3 класс для работы с [Telegram Bot API](https://core.telegram.org/bots/api)
---

Скрипт **find-libcurl&#46;sh** используется классом для поиска библиотеки libcurl в Linux, если она не найдена парсером и не указана вручную. В случае указания пути к libcurl вручную, скрипт поиска не нужен и класс может использоваться в Windows. Для работы скрипта поиска в Linux делаем find-libcurl&#46;sh исполняемым и указываем путь к его директории через опцию **shellScriptDir**, если он будет размещен в другом месте. Необязательные опции закомментированы при помощи ^rem{...} либо явно указано на необязательность. При помощи опции **apiParams** во многие методы можно передавать хеш параметров с именами ключей, соответствующими именам параметров в [Telegram Bot API](https://core.telegram.org/bots/api).

### Инициализация
```php
^use[/classes/telegram/telegram.p]

$telegram[^Telegram::create[
    $.token[telegram_bot_token]
    $.libcurlPath[/path/to/libcurl] ^rem{ необязательно }
    $.shellScriptDir[/../shell-scripts] ^rem{ необязательно, по умолчанию "./" }
]]
```

### Методы

**setWebHook**

```php
$setWebHook[^telegram.setWebHook[
    $.url[https://example.org/telegram-webhook.html]
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#setwebhook]
		]
	}
]]
```

**getUpdates** - получение сообщений вручную

```php
$getUpdates[^telegram.getUpdates[
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#getupdates]
		]
	}
]]
```

**getWebhookUpdate** - получение сообщения в вебхуке

```php
$getWebhookUpdate[^telegram.getWebhookUpdate[]]
```

**sendMessage**

```php
$sendMessage[^telegram.sendMessage[
    $.chatID[123456]
	$.text[Hello world]
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#sendmessage]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**deleteMessage**

```php
$deleteMessage[^telegram.deleteMessage[
    $.chatID[123456]
    $.messageID[123456]
]]
```

**editMessageText**

```php
$editMessageText[^telegram.editMessageText[
    $.chatID[123456]
    $.messageID[123456]
	$.text[Hello world!]
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#editmessagetext]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
	}
]]
```

**sendPhoto**

```php
$sendPhoto[^telegram.sendPhoto[
    $.chatID[123456]
    $.file[^file::load[binary;/images/image.jpg]] ^rem{ file | URL | telegram_file_id }
	^rem{
		$.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#sendphoto]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**sendDocument**

```php
$sendDocument[^telegram.sendDocument[
    $.chatID[123456]
    $.file[^file::load[binary;/documents/document.docx]] ^rem{ file | URL | telegram_file_id }
	^rem{
		$.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#senddocument]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**sendVideo**

```php
$sendVideo[^telegram.sendVideo[
    $.chatID[123456]
    $.file[^file::load[binary;/videos/video.mp4]] ^rem{ file | URL | telegram_file_id }
	^rem{
		$.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#sendvideo]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**sendAudio**

```php
$sendAudio[^telegram.sendAudio[
    $.chatID[123456]
    $.file[^file::load[binary;/audios/audio.mp3]] ^rem{ file | URL | telegram_file_id }
	^rem{
		$.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#sendaudio]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**sendLocation**

```php
$sendLocation[^telegram.sendLocation[
    $.chatID[123456]
	$.latitude[12.3456]
	$.longitude[12.3456]
	^rem{
		$.livePeriod[3600]
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#sendlocation]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
		$.replyKeyboard[$replyKeyboardObject]
	}
]]
```

**editLiveLocation**

```php
$editLiveLocation[^telegram.editLiveLocation[
    $.chatID[123456]
	$.messageID[123456]
	$.latitude[12.3456]
	$.longitude[12.3456]
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#editmessagelivelocation]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
	}
]]
```

**stopLiveLocation**

```php
$stopLiveLocation[^telegram.stopLiveLocation[
    $.chatID[123456]
	$.messageID[123456]
	^rem{
	    $.apiParams[
			$.any_api_params[https://core.telegram.org/bots/api#stopmessagelivelocation]
		]
		$.inlineKeyboard[$inlineKeyboardObject]
	}
]]
```

**getFile**

```php
$getFile[^telegram.getFile[
	$.fileID[telegram_file_id]
	$.saveTo[/path/to/file.ext]
]]
```

### Объекты

**Inline keyboard**

```php
$inlineKeyboardObject[
	$.row1[
		$.btn1[
			$.text[btn text]
			$.callback_data[btn command]
			^rem{ $.other_api_params[https://core.telegram.org/bots/api#inlinekeyboardbutton] }
		]
		^rem{ ... }
	]
	^rem{ ... }
]
```

**Reply keyboard**

```php
$replyKeyboardObject[
	$.row1[
		$.btn1[
			$.text[btn text]
			^rem{ $.other_api_params[https://core.telegram.org/bots/api#keyboardbutton] }
		]
		^rem{ ... }
	]
	^rem{ ... }
]
```

Отсутствующие в классе API-методы можно реализовать подобным образом:

```php
@otherMethod[]

$result[^telegram.apiRequest[
	$.method[telegramApiMethod]
	$.apiParams[
		$.telegram_api_param[telegram_api_value]
	]
]]
```
