@CLASS
Telegram

@OPTIONS
locals
partial


#####################
@sendLocation[params]

# $params.chatID
# $params.latitude
# $params.longitude
# $params.livePeriod [optional (seconds)]
# $params.apiParams [any API params (https://core.telegram.org/bots/api#sendlocation)]

# $params.inlineKeyboard[
#	$.row1[
#		$.btn1[
#			$.text[btn text]
#			$.callback_data[btn command]
#			$.other_api_params[https://core.telegram.org/botspi#inlinekeyboardbutton]
#		]
#		...
#	]
#	...
# ]

^if(
	!def $params
	|| !def $params.chatID
	|| !def $params.latitude
	|| !def $params.longitude
){
	^throw[
		$.type[Telegram]
		$.source[Telegram:sendLocation]
		$.comment[^$params.chatID, ^$params.latitude, ^$params.longitude should be defined]
	]
}

$apiParams[^hash::create[]]
^apiParams.add[$params.apiParams]

$apiParams.chat_id[$params.chatID]
$apiParams.parse_mode[HTML]
$apiParams.latitude[$params.latitude]
$apiParams.longitude[$params.longitude]
$apiParams.live_period[$params.livePeriod]

^if($params.inlineKeyboard){
	$apiParams.replyMarkup[^parseKeyboard[$params.inlineKeyboard;inline]]
}($params.replyKeyboard){
	$apiParams.replyMarkup[^parseKeyboard[$params.replyKeyboard;reply]]
}

$result[^apiRequest[
	$.method[sendLocation]
	$.apiParams[$apiParams]
]]


#########################
@editLiveLocation[params]

# $params.chatID
# $params.messageID
# $params.latitude
# $params.longitude
# $params.apiParams [any API params (https://core.telegram.org/bots/api#editmessagelivelocation)]

# $params.inlineKeyboard[
#	$.row1[
#		$.btn1[
#			$.text[btn text]
#			$.callback_data[btn command]
#			$.other_api_params[https://core.telegram.org/botspi#inlinekeyboardbutton]
#		]
#		...
#	]
#	...
# ]

^if(
	!def $params
	|| !def $params.chatID
){
	^throw[
		$.type[Telegram]
		$.source[Telegram:editLiveLocation]
		$.comment[^$params.chatID should be defined]
	]
}

$apiParams[^hash::create[]]
^apiParams.add[$params.apiParams]

$apiParams.chat_id[$params.chatID]
$apiParams.message_id[$params.messageID]
$apiParams.latitude[$params.latitude]
$apiParams.longitude[$params.longitude]

^if(def $params.inlineKeyboard){
	$apiParams.reply_markup[^parseKeyboard[$params.inlineKeyboard;inline]]
}

$result[^apiRequest[
	$.method[editMessageLiveLocation]
	$.apiParams[$apiParams]
]]


#########################
@stopLiveLocation[params]

# $params.chatID
# $params.messageID
# $params.apiParams [any API params (https://core.telegram.org/bots/api#stopmessagelivelocation)]

# $params.inlineKeyboard[
#	$.row1[
#		$.btn1[
#			$.text[btn text]
#			$.callback_data[btn command]
#			$.other_api_params[https://core.telegram.org/botspi#inlinekeyboardbutton]
#		]
#		...
#	]
#	...
# ]

^if(
	!def $params
	|| !def $params.chatID
	|| !def $params.messageID
){
	^throw[
		$.type[Telegram]
		$.source[Telegram:stopLiveLocation]
		$.comment[^$params.chatID, ^$params.messageID should be defined]
	]
}

$apiParams[^hash::create[]]
^apiParams.add[$params.apiParams]

$apiParams.chat_id[$params.chatID]
$apiParams.message_id[$params.messageID]

^if(def $params.inlineKeyboard){
	$apiParams.reply_markup[^parseKeyboard[$params.inlineKeyboard;inline]]
}

$result[^apiRequest[
	$.method[stopMessageLiveLocation]
	$.apiParams[$apiParams]
]]
