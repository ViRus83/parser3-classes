@CLASS
Telegram

@OPTIONS
locals
partial


##################
@sendMedia[params]

# $params.chatID
# $params.file [url | file | telegram file_id]
# $params.type [audio | document | photo | video]
# $params.apiParams [any API params for corresponding method (https://core.telegram.org/bots/api)]

# $params.inlineKeyboard[
#	$.row1[
#		$.btn1[
#			$.text[btn text]
#			$.callback_data[btn command]
#			$.other_api_params[https://core.telegram.org/bots/api#inlinekeyboardbutton]
#		]
#		...
#	]
#	...
# ]

# $params.replyKeyboard[
#	$.row1[
#		$.btn1[
#			$.text[btn text]
#			$.other_api_params[https://core.telegram.org/bots/api#keyboardbutton]
#		]
#		...
#	]
#	...
# ]

^if(
	!def $params.chatID
	|| !def $params.file
	|| !def $params.type
){
	^throw[
		$.type[Telegram]
		$.source[Telegram:sendMedia]
		$.comment[^$params.chatID, ^$params.file, ^$params.type should be defined]
	]
}

$apiParams[^hash::create[]]
^apiParams.add[$params.apiParams]
$apiParams.chat_id[$params.chatID]

$mediaType[$params.type]

^if($params.file is string){
	$apiParams.[$mediaType][$params.file]
}

$type[^params.type.lower[]]
$typeCapitalized[^type.match[(?:_|^^)(\S)][g]{^match.1.upper[]}]

$result[^apiRequest[

	$.method[send$typeCapitalized]
	$.apiParams[$apiParams]

	^if($params.file is file){
		$.post[
			$.[$mediaType][$params.file]
		]
	}

]]


##################
@sendPhoto[params]

$params.type[photo]
$result[^sendMedia[$params]]


#####################
@sendDocument[params]

$params.type[document]
$result[^sendMedia[$params]]


##################
@sendVideo[params]

$params.type[video]
$result[^sendMedia[$params]]


##################
@sendAudio[params]

$params.type[audio]
$result[^sendMedia[$params]]


################
@getFile[params]

# $params.fileID
# $params.saveTo [/path/to/file.ext]

^if(
	!def $params
	|| !def $params.fileID
	|| !def $params.saveTo
){
	^throw[
		$.type[Telegram]
		$.source[Telegram:getFile]
		$.comment[^$params.fileID, ^$params.saveTo should be defined]
	]
}

$apiRequest[^apiRequest[
	$.method[getFile]
	$.apiParams[
		$.file_id[$params.fileID]
	]
]]

$apiResponse[^parseJson[$apiRequest.text]]
$filePath[$apiResponse.result.file_path]

$downloadFile[^curl:load[
	$.url[https://api.telegram.org/file/bot$self.token/$filePath]
	$.mode[binary]
	$.ssl_verifypeer(0)
]]

^downloadFile.save[binary;$params.saveTo]

$result[
	$.info[$apiRequest]
	$.file[$downloadFile]
]
