#!/bin/sh

# $1: local dir
# $2: remote file
# $3: user
# $4: password
# $5: locale [optional]

if ! [ -z ${5+x} ]; then
	export LANG=$5;
fi;

curl -o $1 $2 --user $3:$4
