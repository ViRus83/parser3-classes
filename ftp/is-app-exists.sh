#!/bin/bash

# $1 - app name

allowedApps=(wget curl)

if [[ " ${allowedApps[*]} " =~ " ${1} " ]]; then
	command -v $1
fi;
