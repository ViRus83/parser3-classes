#!/bin/sh

# $1: local file
# $2: remote dir
# $3: user
# $4: password
# $5: locale [optional]

if ! [ -z ${5+x} ]; then
	export LANG=$5;
fi;

curl -T $1 $2 --ftp-create-dirs --user $3:$4
