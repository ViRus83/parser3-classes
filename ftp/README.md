# Parser3 класс для работы с FTP через cURL и Wget
---

Все операции осуществляются при помощи cURL, кроме скачивания директорий методом **get**. В этом случае будет использован Wget, если он есть. Директории Wget качает быстрее, чем cURL. Не забываем сделать sh-скрипты исполняемыми и указать к ним путь через опцию **shellScriptDir**, если они будут размещены в другой директории.

### Требования
- *nix
- cURL
- Wget (необязательно)

### Инициализация
```php
^use[/classes/ftp/ftp.p]

$ftp[^Ftp::create[
    $.host[ftp.domain.com]
    $.user[xxx]
    $.password[xxx]
    $.locale[ru_RU.UTF-8] ^rem{ необязательно }
    $.shellScriptDir[/../shell-scripts/ftp] ^rem{ необязательно, по умолчанию "./" }
]]
```

### Методы


**get** - рекурсивное скачивание директорий или файлов.

```php
$get[^ftp.get[
    $.local[/local/dir]
    $.remote[/remote/dir/or/file]
]]
```

**list** - получение листинга директории на удаленном сервере.

```php
$list[^ftp.list[
    $.remote[/remote/dir]
]]
```

** delete** - рекурсивное удаление директорий или файлов на удаленном сервере.
```php
$delete[^ftp.delete[
    $.remote[/remote/dir/or/file]
]]
```

**put** - рекурсивная выгрузка директорий или файлов на удаленный сервер.

```php
$put[^ftp.put[
    $.local[/local/dir/or/file]
    $.remote[/remote/dir]
]]
```
