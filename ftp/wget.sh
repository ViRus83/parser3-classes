#!/bin/sh

# $1: local dir
# $2: remote dir
# $3: user
# $4: password
# $5: cut dirs
# $6: locale [optional]

if ! [ -z ${6+x} ]; then
	export LANG=$6;
fi;

if [ -z ${5+x} ]; then
	cutDirs=""
else
	cutDirs=$5
fi

wget -r -nH --no-parent --cut-dirs=$cutDirs $2 --ftp-user=$3 --ftp-password=$4 -P $1
