#!/bin/sh

# $1: remote path
# $2: user
# $3: password
# $4: locale [optional]

if ! [ -z ${4+x} ]; then
	export LANG=$4;
fi;

curl --list-only $1 --user $2:$3
