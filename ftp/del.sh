#!/bin/sh

# $1: user
# $2: password
# $3: host
# $4: remote dir or file
# $5: file | dir
# $6: locale [optional]

if ! [ -z ${5+x} ]; then
	export LANG=$5;
fi;

if [ "$5" = "file" ]; then
	curl $3 --user $1:$2 -Q "DELE $4"
elif [ "$5" = "dir" ]; then
	curl $3 --user $1:$2 -Q "RMD $4"
fi;
